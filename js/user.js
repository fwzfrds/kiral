(function ($) {
    'use strict';

    var baseUrl = window.location.origin + window.location.pathname.trimEnd("/");

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    $('#tombol-tambah').click(function () {
        $('#button-simpan').val("create-post"); //valuenya menjadi create-post
        $('#id').val(''); //valuenya menjadi kosong
        $('#form-tambah-edit').trigger("reset"); //mereset semua input dll didalamnya
        $('#modal-judul').html("Tambah User"); //valuenya tambah pegawai baru
        $('#tambah-edit-modal').modal('show'); //modal tampil
    });

    $(document).ready(function () {
        $('#users').dataTable({
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: `${baseUrl}`,
                type: 'GET',
                // success: function (data) {
                //     console.log(data);
                // },
                // error: function (err) {
                //     console.log(err);
                // }
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'roles',
                    name: 'roles'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ]
        })
    })

    // Simpan - Update function
    if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
            submitHandler: function (form) {
                var actionType = $('#tombol-simpan').val();
                $('#tombol-simpan').html('Sending..');

                $.ajax({
                    data: $('#form-tambah-edit').serialize(),
                    url: `${baseUrl}`,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        $('#form-tambah-edit').trigger("reset");
                        $('#tambah-edit-modal').modal('hide');
                        $('#form-simpan').html('Simpan');
                        var oTable = $('#table_users').dataTable();
                        oTable.fnDraw(false);
                        iziToast.success({
                            title: 'User Berhasil Dismpan',
                            position: 'bottomCenter'
                        });
                    },
                    error: function (data) {
                        console.log('Error', data);
                        $('#tombol-simpan').html('Simpan');
                    }
                });
            }
        });
    }

    // $('body').on('click', '.edit-post', function () {
    //     let data_id = $(this).data('id');
    //     $.get(`category-age/${data_id}`, function (data) {
    //         $('#modal-judul').html("Edit User");
    //         $('#tombol-simpan').val("edit-post");
    //         $('#tambah-edit-modal').modal('show');

    //         //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
    //         $('#id').val(data.id);
    //         $('#category_age_name').val(data.category_age_name);
    //     })
    // })

    // $(document).on('click', '.delete', function () {
    //     let dataId = $(this).attr('id');
    //     swal({
    //         title: "Perhatian",
    //         text: "Apakah anda Yakin ingin mengahpus Kategori umur ini?",
    //         icon: "warning",
    //         buttons: true,
    //         dangerMode: true,
    //     })
    //         .then((willDelete) => {
    //             if (willDelete) {
    //                 $.ajax({
    //                     url: `${baseUrl}/${dataId}`,
    //                     type: 'delete',
    //                     success: function (data) {
    //                         var oTable = $('#category_age').dataTable();
    //                         oTable.fnDraw(false);
    //                         swal("OK! Kategori umur telah terhapus!", {
    //                             icon: "success",
    //                         });
    //                     }
    //                 })
    //             }
    //         })
    // })

})(window.jQuery);

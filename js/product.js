(function ($) {
    'use strict';

    var baseUrl = window.location.origin + window.location.pathname.trimEnd("/");
    var url = window.location.origin.trimEnd("/");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#tombol-tambah').click(function () {
        $('#button-simpan').val("create-post"); //valuenya menjadi create-post
        $('#id').val(''); //valuenya menjadi kosong
        $('#form-tambah-edit').trigger("reset"); //mereset semua input dll didalamnya
        $('#modal-judul').html("Tambah Produk"); //valuenya tambah pegawai baru
        $('#tambah-edit-modal').modal('show'); //modal tampil
    });


    $(document).ready(function () {
        $('#products').dataTable({
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: `${baseUrl}`,
                type: 'GET',
                // success: function (data) {
                //     console.log(data);
                // },
                // error: function (err) {
                //     console.log(err);
                // }
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },
                {
                    data: 'product_name',
                    name: 'product_name'
                },
                {
                    data: 'size',
                    name: 'size'
                },
                {
                    data: 'images',
                    name: 'images',
                    render: function (data, name) {
                        return `<img src="${url}/images/${data}" alt="${name}" class="img-thumbnail"/>`
                    }
                },
                {
                    data: 'images_2',
                    name: 'images_2',
                    render: function (data, name) {
                        return `<img src="${url}/images/${data === null ? 'logo_default.jpeg' : data}" alt="${name}" class="img-thumbnail"/>`
                    }
                },
                {
                    data: 'images_3',
                    name: 'images_3',
                    render: function (data, name) {
                        return `<img src="${url}/images/${data === null ? 'logo_default.jpeg' : data}" alt="${name}" class="img-thumbnail"/>`
                    }
                },
                {
                    data: 'images_4',
                    name: 'images_4',
                    render: function (data, name) {
                        return `<img src="${url}/images/${data === null ? 'logo_default.jpeg' : data}" alt="${name}" class="img-thumbnail"/>`
                    }
                },
                {
                    data: 'images_5',
                    name: 'images_5',
                    render: function (data, name) {
                        return `<img src="${url}/images/${data === null ? 'logo_default.jpeg' : data}" alt="${name}" class="img-thumbnail"/>`
                    }
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ]
        })
    })

    // Simpan - Update function
    $('#form-tambah-edit').submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);

        console.log(formData)

        $.ajax({
            type: 'POST',
            url: `${baseUrl}`,
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    this.reset();
                    $('#form-tambah-edit').trigger("reset");
                    $('#tambah-edit-modal').modal('hide');
                    $('#form-simpan').html('Simpan');
                    var oTable = $('#products').dataTable();
                    oTable.fnDraw(false);
                    iziToast.success({
                        title: 'Produk Berhasil Dismpan',
                        position: 'bottomCenter'
                    });
                }
            },
            error: function (response) {
                console.log(response.responseJSON.message);
                $('#product-name-input-error').text(response.responseJSON.errors.product_name);
                $('#image-input-error').text(response.responseJSON.errors.images);
                iziToast.error({
                    title: 'Produk Gagal disimpan',
                    meesage: response.responseJSON.errors,
                    position: 'bottomCenter'
                });
            }
        })
    });

    $('body').on('click', '.edit-post', function () {
        let data_id = $(this).data('id');
        $.get(`products/${data_id}`, function (data) {
            $('#modal-judul').html("Edit Produk");
            $('#tombol-simpan').val("edit-post");
            $('#tambah-edit-modal').modal('show');

            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#product_name').val(data.product_name);
            $('#description').val(data.description);
            $('#size').val(data.size);
            $('#material').val(data.material);
            if (data.category_gender_id) {
                $(`#category_gender_id option[value='${data.category_gender_id}']`).attr("selected", "selected");
                //  $("#drprojecttype").val("Dedicated");
            }
            $('#category_product_id').val(data.category_product_id);
            $('#category_age').val(data.category_age);
            $('#images').val(data.images);
        })
    })

    $(document).on('click', '.delete', function () {
        let dataId = $(this).attr('id');
        swal({
            title: "Perhatian",
            text: "Apakah anda Yakin ingin mengahpus Produk ini?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: `${baseUrl}/${dataId}`,
                        type: 'delete',
                        success: function (data) {
                            var oTable = $('#products').dataTable();
                            oTable.fnDraw(false);
                            swal("OK! Produk telah terhapus!", {
                                icon: "success",
                            });
                        }
                    })
                }
            })
    })

})(window.jQuery);

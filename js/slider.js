(function ($) {
    'use strict';
    var url = window.location.origin.trimEnd("/");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#tombol-tambah').click(function () {
        $('#button-simpan').val("create-post");
        $('#id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#modal-judul').html("Tambah Slider");
        $('#tambah-edit-modal').modal('show');
    });

    $(document).ready(function () {
        $('#slider').dataTable({
            autoWidth: false,
            responsive: true,
            processing: true,
            serverSide: true,
            pagination: true,
            ajax: {
                url: `${url}/slider`,
                type: 'GET',
                dataSrc: '',
                // success: function (data) {
                //     console.log(data)
                // },
                // error: function (err) {
                //     console.log(err);
                // }
            },
            columns: [
                {
                    data: 'id',
                    name: 'id',
                },
                {
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'tagline',
                    name: 'tagline'
                },
                {
                    data: 'images',
                    name: 'images',
                    render: function (data, name) {
                        return `<img src="${url}/images/slide/${data}" alt="${name}" class="img-thumbnail"/>`
                    }
                },
                {
                    data: 'id',
                    name: 'id',
                    render: function (data) {
                        return `
                        '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="${data}" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"> Edit</a>&nbsp;&nbsp
                        <button type="button" name="delete" id="${data}" class="delete btn btn-danger btn-sm"> Delete</button>
                        `
                    }
                },
            ],
        })
    })

    // Simpan - Update function
    $('#form-tambah-edit').submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: `${url}/slider`,
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    this.reset();
                    $('#form-tambah-edit').trigger("reset");
                    $('#tambah-edit-modal').modal('hide');
                    $('#form-simpan').html('Simpan');
                    var oTable = $('#slider').dataTable();
                    oTable.fnDraw(false);
                    iziToast.success({
                        title: 'Slider Berhasil Dismpan',
                        position: 'bottomCenter'
                    });
                }
            },
            error: function (response) {
                console.log(response.responseJSON.message);
                $('#title-name-input-error').text(response.responseJSON.errors.title);
                $('#image-input-error').text(response.responseJSON.errors.images);
                iziToast.error({
                    title: 'Slider Gagal disimpan',
                    message: response.responseJSON.errors,
                    position: 'bottomCenter'
                });
            }
        })
    });

    $('body').on('click', '.edit-post', function () {
        let data_id = $(this).data('id');
        $.get(`slider/${data_id}`, function (data) {
            $('#modal-judul').html("Edit Produk");
            $('#tombol-simpan').val("edit-post");
            $('#tambah-edit-modal').modal('show');

            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#title').val(data.title);
            $('#tagline').val(data.tagline);

            $('#images').val(data.images);
        })
    })

    $(document).on('click', '.delete', function () {
        let dataId = $(this).attr('id');
        swal({
            title: "Perhatian",
            text: "Apakah anda Yakin ingin mengahpus Slide ini?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: `${url}/slider/${dataId}`,
                        type: 'delete',
                        success: function (data) {
                            var oTable = $('#slider').dataTable();
                            oTable.fnDraw(false);
                            swal("OK! Produk telah terhapus!", {
                                icon: "success",
                            });
                        },
                        error: function (err) {
                            var oTable = $('#slider').dataTable();
                            oTable.fnDraw(false);
                            swal("Opps! Produk telah terhapus!", {
                                icon: "error",
                            });
                        }
                    })
                }
            })
    })

})(window.jQuery);
